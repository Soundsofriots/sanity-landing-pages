export default {
  widgets: [
    {
      name: 'sanity-tutorials',
      options: {
        templateRepoId: 'sanity-io/sanity-template-nextjs-landing-pages'
      }
    },
    {name: 'structure-menu'},
    {
      name: 'project-info',
      options: {
        __experimental_before: [
          {
            name: '',
            options: {
              description:
                ' ',
              sites: [
                {
                  buildHookId: '6000ce5414eaf31027ab81e1',
                  title: 'Admin dashboard',
                  name: 'sanity-landing-pages-studio-o3ocayjk',
                  apiId: '40000ef1-8caa-4148-9060-057aa8b34afd'
                },
                {
                  buildHookId: '6000ce5410c9890102a38f3e',
                  title: 'Integrated Systems',
                  name: 'sanity-landing-pages-web-1fumkypr',
                  apiId: 'da8de931-325b-43b5-aa6f-391b4d5d4964'
                }
              ]
            }
          }
        ],
        data: [
          {
            title: 'GitHub repo',
            value: 'https://github.com/georgenaranjo96/sanity-landing-pages',
            category: 'Code'
          },
          {title: 'Frontend', value: 'https://sanity-landing-pages-web-1fumkypr.netlify.app', category: 'apps'}
        ]
      }
    },
    {name: 'project-users', layout: {height: 'auto'}},
    {
      name: 'document-list',
      options: {title: 'Recently edited', order: '_updatedAt desc', limit: 10, types: ['page']},
      layout: {width: 'medium'}
    }
  ]
}

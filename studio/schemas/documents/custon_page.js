export default {
  name: 'page',
  type: 'document',
  title: 'Custom Page',
  fieldsets: [
    {
      title: 'title'
    },
  ],
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title',
    },
    {
      name: 'content',
      type: 'array',
      title: 'Page sections',
      of: [
        { type: 'hero' },
        { type: 'imageSection' },
        { type: 'mailchimp' },
        { type: 'textSection' },
      ],
    },
  ],
};
